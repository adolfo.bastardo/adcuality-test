<?php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class RandomStatisticsTest extends WebTestCase
{
    public function testRandomByMonthsSuccessful() {
        $client = static::createClient();

        $client->request('GET', '/random/by-month/2020');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $this->assertNotEquals(false, $content);
        $this->assertJson($content);

        $contentDecoded = json_decode($content);


        $this->assertIsInt($contentDecoded->year);
        $this->assertEquals(2020, $contentDecoded->year);
        $this->assertIsNumeric($contentDecoded->months->sep);
    }

    public function testRandomByMonthsNotValidInt() {
        $client = static::createClient();

        $client->request('GET', '/random/by-month/-10');

        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $this->assertNotEquals(false, $content);
        $this->assertJson($content);

        $contentDecoded = json_decode($content);
        $this->assertEquals("El año debe ser un número positivo mayor a 0", $contentDecoded->message);
    }

    public function testRandomByMonthsNotInt() {
        $this->expectException(\TypeError::class);
        $client = static::createClient();
        $client->catchExceptions(false);

        $client->request('GET', '/random/by-month/a');

        $this->assertEquals(500, $client->getResponse()->getStatusCode());
    }

    public function testRandomByDaysSuccessful() {
        $client = static::createClient();

        $client->request('GET', '/random/by-day/3');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $this->assertNotEquals(false, $content);
        $this->assertJson($content);

        $contentDecoded = json_decode($content);


        $this->assertIsInt($contentDecoded->month);
        $this->assertEquals(3, $contentDecoded->month);
        $this->assertIsNumeric($contentDecoded->days->{"31"});
    }

    public function testRandomByDaysNotValidInt() {
        $client = static::createClient();

        $client->request('GET', '/random/by-day/33');

        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $this->assertNotEquals(false, $content);
        $this->assertJson($content);

        $contentDecoded = json_decode($content);
        $this->assertEquals("El mes debe ser un número positivo entre 1 y 12", $contentDecoded->message);
    }

    public function testRandomByDaysNotInt() {
        $this->expectException(\TypeError::class);
        $client = static::createClient();
        $client->catchExceptions(false);

        $client->request('GET', '/random/by-day/a');

        $this->assertEquals(500, $client->getResponse()->getStatusCode());
    }
}