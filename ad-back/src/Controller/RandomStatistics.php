<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class RandomStatistics extends AbstractController
{
    // TODO: Documentation
    public function randomByMonths(int $year) {
        if ($year < 1) {
            $error = [
                "message" => "El año debe ser un número positivo mayor a 0",
            ];
            return $this->json($error, 400);
        }

        $data = [
            "year" => $year,
            "months" => [
                "jan" => rand(1, 1000),
                "feb" => rand(1, 1000),
                "mar" => rand(1, 1000),
                "apr" => rand(1, 1000),
                "may" => rand(1, 1000),
                "jun" => rand(1, 1000),
                "jul" => rand(1, 1000),
                "aug" => rand(1, 1000),
                "sep" => rand(1, 1000),
                "oct" => rand(1, 1000),
                "nov" => rand(1, 1000),
                "dec" => rand(1, 1000)
            ]
        ];
        return $this->json($data, 200);
    }

    // TODO: Documentation
    public function randomByDays(int $month) {
        if ($month < 1 || $month > 12) {
            $error = [
                "message" => "El mes debe ser un número positivo entre 1 y 12",
            ];
            return $this->json($error, 400);
        }
        $interval = range(1, cal_days_in_month(CAL_GREGORIAN, $month, 2020));

        $daysArr = [];

        foreach ($interval as $i) {
            $daysArr["$i"] = rand(500, 800);
        }

        $data = [
            "month" => $month,
            "days" => $daysArr
        ];
        return $this->json($data, 200);
    }
}