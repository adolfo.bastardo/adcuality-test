# adcuality-test

Luego de clonar este proyecto, dirigirse a la carpeta creada a través de la consola/terminal

## Configurar backend

Para poder ejecutar el backend es necesario tener instalados:

    - Docker
    - docker-compose
    - PHP (Solo para instalar dependecias)
    - Composer (Solo para instalar dependecias)

Teniendo estos programas instalados debemos ejecutar los siguientes comandos:

```bash
$ cd ad-back && composer install
$ docker-compose up -d
```

Ya así tenemos nuestro backend corriendo bajo la url: (http://localhost:8000)[http://localhost:8000]



## Configurar frontend

Ahora para poder ejecutar el frontend solo es necesario tener instalados:

    - Node.js
    - npm o yarn (en mi caso usé yarn)

Teniendo estos programas instalados debemos ejecutar los siguientes comandos:

```bash
$ cd ../ad-front
$ yarn install && yarn start
```

Ya así tenemos nuestro front corriendo bajo la url: (http://localhost:3000)[http://localhost:3000]

PD: También iba a usar docker acá pero como se acabó el tiempo no pude

