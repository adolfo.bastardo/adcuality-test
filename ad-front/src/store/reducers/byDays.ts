import { DaysState, ADD_DATA_BY_DAYS, DELETE_DATA_BY_DAYS, DataDaysActions } from '../types';

export const DaysInitialState: DaysState = {
    statistics: []
}

export const daysReducer = (state=DaysInitialState, action: DataDaysActions): DaysState => {
    switch (action.type) {
        case ADD_DATA_BY_DAYS:
            const isHere = state.statistics.some(statistic => statistic.month === action.payload.month);
            return !isHere ? {
                statistics: [...state.statistics, action.payload]
            } : state
        case DELETE_DATA_BY_DAYS:
            return {
                statistics: state.statistics.filter(statistic => statistic.month !== action.meta.month)
            }
        default:
            return state
    }
};