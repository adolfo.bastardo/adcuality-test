import { MonthsState, ADD_DATA_BY_MONTHS, DELETE_DATA_BY_MONTHS, DataMonthsActions } from '../types';

export const MonthsInitialState: MonthsState = {
    statistics: []
}

export const monthsReducer = (state=MonthsInitialState, action: DataMonthsActions): MonthsState => {
    switch (action.type) {
        case ADD_DATA_BY_MONTHS:
            const isHere = state.statistics.some(statistic => statistic.year === action.payload.year);
            return !isHere ? {
                statistics: [...state.statistics, action.payload]
            } : state
        case DELETE_DATA_BY_MONTHS:
            return {
                statistics: state.statistics.filter(statistic => statistic.year !== action.meta.year)
            }
        default:
            return state
    }
};
