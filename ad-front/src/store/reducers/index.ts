import { combineReducers } from 'redux';
import { monthsReducer } from './byMonth';
import { daysReducer } from './byDays';

export const rootReducer = combineReducers({
    byMonth: monthsReducer,
    byDays: daysReducer
});

export type RootState = ReturnType<typeof rootReducer>;
