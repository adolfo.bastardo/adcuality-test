import { DataMonths, ADD_DATA_BY_MONTHS, DELETE_DATA_BY_MONTHS, DataMonthsActions } from '../types';
import { DataDays, ADD_DATA_BY_DAYS, DELETE_DATA_BY_DAYS, DataDaysActions } from '../types';

export const addMonths = (newData: DataMonths): DataMonthsActions => {
    return {
        type: ADD_DATA_BY_MONTHS,
        payload: newData
    }
}

export const deleteMonths = (year: number): DataMonthsActions => {
    return {
        type: DELETE_DATA_BY_MONTHS,
        meta: {
            year: year
        }
    }
}

export const addDays = (newData: DataDays): DataDaysActions => {
    return {
        type: ADD_DATA_BY_DAYS,
        payload: newData
    }
}

export const deleteDays = (month: number): DataDaysActions => {
    return {
        type: DELETE_DATA_BY_DAYS,
        meta: {
            month: month
        }
    }
}
