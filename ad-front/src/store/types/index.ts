export interface Months {
    jan: number;
    feb: number;
    mar: number;
    apr: number;
    may: number;
    jun: number;
    jul: number;
    aug: number;
    sep: number;
    oct: number;
    nov: number;
    dec: number;
}

export interface DataMonths {
    year: number;
    months: Months
}

export interface MonthsState {
    statistics: DataMonths[];
}


export interface Days {
    1: number;
    2: number;
    3: number;
    4: number;
    5: number;
    6: number;
    7: number;
    8: number;
    9: number;
    10: number;
    11: number;
    12: number;
    13: number;
    14: number;
    15: number;
    16: number;
    17: number;
    18: number;
    19: number;
    20: number;
    21: number;
    22: number;
    23: number;
    24: number;
    25: number;
    26: number;
    27: number;
    28: number;
    29?: number;
    30?: number;
    31?: number;
}

export interface DataDays {
    month: number;
    days: Days;
}

export interface DaysState {
    statistics: DataDays[];
}

export const ADD_DATA_BY_MONTHS = 'ADD_DATA_BY_MONTHS';
export const DELETE_DATA_BY_MONTHS = 'DELETE_DATA_BY_MONTHS';
export const ADD_DATA_BY_DAYS = 'ADD_DATA_BY_DAYS';
export const DELETE_DATA_BY_DAYS = 'DELETE_DATA_BY_DAYS';

interface AddDataByMonthsAction {
    type: typeof ADD_DATA_BY_MONTHS;
    payload: DataMonths
}

interface DeleteDataByMonthsAction {
    type: typeof DELETE_DATA_BY_MONTHS;
    meta: {
        year: number;
    };
}

interface AddDataByDaysAction {
    type: typeof ADD_DATA_BY_DAYS;
    payload: DataDays
}

interface DeleteDataByDaysAction {
    type: typeof DELETE_DATA_BY_DAYS;
    meta: {
        month: number;
    };
}

export type DataMonthsActions = AddDataByMonthsAction | DeleteDataByMonthsAction;
export type DataDaysActions = AddDataByDaysAction | DeleteDataByDaysAction;
