import React from 'react';
import { Provider } from 'react-redux';
import CssBaseLine from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import './App.scss';

import {store} from './store';

import StatisticsByMonth from './components/StatisticsByMonth/StatisticsByMonth';
import StatisticsByDay from './components/StatisticsByDay/StatisticsByDay';

interface StatisticsTabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

const StatisticsTabPanel = (props: StatisticsTabPanelProps) => {
  const { children, value, index, ...other } = props;

  return (
    <Box
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`statistics-tabpanel-${index}`}
      aria-labelledby={`statistics-tab-${index}`}
      {...other}
    >
      {value === index ? (children) : false}
    </Box>
  );
}

function a11yProps(index: any) {
  return {
    id: `statistics-tab-${index}`,
    'aria-controls': `statistics-tabpanel-${index}`,
  };
}

function App() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Provider store={store}>
      <div className="App">
        <CssBaseLine />
        <Container maxWidth="xl">
          <Grid container spacing={3} alignContent="center">
            <Grid item xs={12}>
              <Typography variant="h3" style={{ textAlign: 'center', padding: '1em 0' }}>Prueba de AdCuality - Estadísticas Random</Typography>
            </Grid>
            <Grid item xs={12}>
              <AppBar position="static">
                <Tabs
                  value={value}
                  onChange={handleChange}
                  aria-label="Statistics tab"
                  variant="fullWidth"
                >
                  <Tab label="Por mes en un año" {...a11yProps(0)} />
                  <Tab label="Por día en un mes" {...a11yProps(1)} />
                </Tabs>
              </AppBar>
              <StatisticsTabPanel value={value} index={0}>
                <StatisticsByMonth />
              </StatisticsTabPanel>
              <StatisticsTabPanel value={value} index={1}>
                <StatisticsByDay />
              </StatisticsTabPanel>
            </Grid>
          </Grid>
        </Container>
      </div>
    </Provider>
  );
}

export default App;
