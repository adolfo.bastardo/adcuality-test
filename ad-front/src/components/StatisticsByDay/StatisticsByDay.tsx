import React, {useState, useEffect, FormEvent, ChangeEvent} from 'react';
import { connect, ConnectedProps, useDispatch, useSelector, shallowEqual } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import MaterialTable from 'material-table';
import Chart from "react-apexcharts";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import { RootState } from '../../store/reducers';
import { DataDays } from '../../store/types';

const Alert = React.forwardRef((props: AlertProps, ref?: React.Ref<JSX.Element>) => {
  return <MuiAlert elevation={6} variant="filled" {...props} ref={ref} />;
});

const getMonthName = (val: number) => {
    const months = [
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre'
    ];

    return  months[val];
}

const mapDaysState = (state: RootState) => ({
    byDays: state.byDays
});

const mapDaysDispatch = {
    addDays: (data: DataDays) => ({
        type: 'ADD_DATA_BY_DAYS',
        payload: data
    }),
    deleteDays: (month: number) => ({
        type: 'DELETE_DATA_BY_DAYS',
        meta: {
            month: month
        }
    })
};

const connector = connect(
    mapDaysState,
    mapDaysDispatch
);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {};

const StatisticsByDay = (props: Props) => {
    const dataStored = useSelector((state: RootState) => state.byDays.statistics, shallowEqual);
    const dispatch = useDispatch();
    const [monthInput, setMonthInput] = useState<number | undefined>(undefined);
    const [tableData, setTableData] = useState<Array<{}>>([]);
    const [optionsBar, setOptionsBar] = useState({});
    const [seriesBar, setSeriesBar] = useState<Array<{
        name: string,
        data: Array<number>
    }>>([]);
    const [labelsPie, setlabelsPie] = useState<Array<string>>([]);
    const [seriesPie, setSeriesPie] = useState<Array<number>>([]);
    const [openSnack, setOpenSnack] = useState(false);
    const [messageSnack, setMessageSnack] = useState<string>('');

    const mockColums: Array<{
        [x: string]: {};
    }> = [
        {
            title: `Mes`,
            field: `month`
        }
    ];
    for (let i = 1; i <= 31; i++) {
        mockColums.push({
            title: `Día ${i}`,
            field: `${i}`,
            type: 'numeric'
        });
    }

    const handleMonth = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const target = event.target;
        if (target) {
            const value = target.value ? parseInt(target.value, 10) : undefined;
            setMonthInput(value);
        }
    };

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const dataGet = dataStored.some(data => data.month === monthInput);
        if (!dataGet) {
            fetch(
                `http://localhost:8000/random/by-day/${monthInput}`,
                {
                    method: 'GET'
                }
            )
            .then(res => res.json())
            .catch(err => {
                console.log(err);
            })
            .then(response => {
                console.log(response);
                if (response && response.month && response.days) {
                    const data: DataDays = {
                        month: response.month,
                        days: response.days
                    };
                    console.log('antes del dispatch');
                    dispatch(props.addDays(data));
                } else {
                    const message = response && response.message ? response.message : 'Error al consultar el año';
                    setMessageSnack(message);
                    setOpenSnack(true);
                }
            });
        } else {
            setMessageSnack('Ya se ha buscado este mes');
            setOpenSnack(true);
        }
    };

    const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenSnack(false);
    };

    useEffect(() => {
        if (dataStored && dataStored[0] && dataStored[0].days) {
            const tableDataGet: Array<any> = [];

            dataStored.forEach(data => {
                const newRow = {
                    ...data.days,
                    month: getMonthName(data.month - 1)
                };
                tableDataGet.push(newRow);
            });
            setTableData(tableDataGet);
        }
    }, [dataStored]);

    const updateCharts = (event: any, rowData: any) => {
        const dataGet = dataStored.find(data => getMonthName(data.month - 1) === rowData.month);
        if (dataGet) {
            const mockOptions = {
                chart: {
                    id: "basic-bar"
                },
                xaxis: {
                    categories: Object.keys(dataGet.days).map(key => key)
                }
            };
            setOptionsBar(mockOptions);
            const mockSeries: Array<{
                name: string,
                data: Array<number>
            }> = [];
            const mockPieLabels: Array<string> = Object.keys(dataGet.days).map(key => key);
            const mockPieSeries: Array<number> = Object.values(dataGet.days);
            mockSeries.push({
                name: `series-${dataGet.month}`,
                data: Object.values(dataGet.days)
            });
    
            setSeriesBar(mockSeries);
            setlabelsPie(mockPieLabels);
            setSeriesPie(mockPieSeries);
        }
    };

    return (
        <>
            <Grid container spacing={3} justify="center">
                <Grid container item xs={12} justify="center" spacing={1} style={
                    {
                        padding: '2em 0'
                    }
                }>
                    <Grid item xs={12} lg={6}>
                        <form noValidate autoComplete="off" onSubmit={handleSubmit}>
                            <TextField
                                id="month"
                                name="month"
                                required
                                label="Ingrese el mes a buscar"
                                type="number"
                                onChange={handleMonth}
                                style={{
                                    width: '70%',
                                    marginRight: '10%'
                                }}
                            />
                            <Button
                                variant="contained"
                                color="primary"
                                endIcon={<Icon>send</Icon>}
                                type="submit"
                                style={{
                                    width: '20%'
                                }}
                            >
                                Buscar
                            </Button>
                        </form>
                    </Grid>
                </Grid>
                {
                    tableData && tableData[0] ? (
                        <Grid item xs={12}>
                            <div style={{ maxWidth: '100%' }}>
                                <MaterialTable
                                    columns={mockColums}
                                    data={tableData}
                                    title="Por día"
                                    actions={[
                                        {
                                        icon: 'visibility',
                                        tooltip: 'Mostrar gráficos',
                                        onClick: updateCharts
                                        },
                                        {
                                        icon: 'delete',
                                        tooltip: 'Borrar fila',
                                        onClick: (event, rowData: any) => dispatch(props.deleteDays(rowData.month))
                                        }
                                    ]}
                                />
                            </div>
                        </Grid>
                    )
                    :
                    ''
                }
                {
                    optionsBar && seriesBar && seriesBar[0] && labelsPie && seriesPie && seriesPie[0] ? (
                        <>
                            <Grid item xs={12} lg={6}>
                                <Chart
                                    options={optionsBar}
                                    series={seriesBar}
                                    type="bar"
                                    width="100%"
                                />
                            </Grid>
                            <Grid item xs={12} lg={6}>
                                <Chart
                                    options={{charOptions: labelsPie}}
                                    series={seriesPie}
                                    type="pie"
                                    width="100%"
                                />
                            </Grid>
                        </>
                    ) : ''
                }
            </Grid>
            <Snackbar open={openSnack} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error">
                    {messageSnack}
                </Alert>
            </Snackbar>
        </>
    );
}


export default connector(StatisticsByDay);
