import React, {useState, useEffect, FormEvent, ChangeEvent, useRef} from 'react';
import { connect, ConnectedProps, useDispatch, useSelector, shallowEqual } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import MaterialTable from 'material-table';
import Chart from "react-apexcharts";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import { RootState } from '../../store/reducers';
import { DataMonths } from '../../store/types';

const Alert = React.forwardRef((props: AlertProps, ref?: React.Ref<JSX.Element>) => {
  return <MuiAlert elevation={6} variant="filled" {...props} ref={ref} />;
});

const getMonthName = (val: string) => {
    const months: {[key: string]: string} = {
        jan: 'Enero',
        feb: 'Febrero',
        mar: 'Marzo',
        apr: 'Abril',
        may: 'Mayo',
        jun: 'Junio',
        jul: 'Julio',
        aug: 'Agosto',
        sep: 'Septiembre',
        oct: 'Octubre',
        nov: 'Noviembre',
        dec: 'Diciembre'
    }

    return  months[val];
}

const mapMonthsState = (state: RootState) => ({
    byMonth: state.byMonth
});

const mapMonthsDispatch = {
    addMonths: (data: DataMonths) => ({
        type: 'ADD_DATA_BY_MONTHS',
        payload: data
    }),
    deleteMonths: (year: number) => ({
        type: 'DELETE_DATA_BY_MONTHS',
        meta: {
            year: year
        }
    })
};

const connector = connect(
    mapMonthsState,
    mapMonthsDispatch
);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {};

const StatisticsByMonth = (props: Props) => {
    const dataStored = useSelector((state: RootState) => state.byMonth.statistics, shallowEqual);
    const dispatch = useDispatch();
    const [tableData, setTableData] = useState<Array<{}>>([]);
    const [yearInput, setYearInput] = useState<number | undefined>(undefined);
    const [optionsBar, setOptionsBar] = useState({});
    const [seriesBar, setSeriesBar] = useState<Array<{
        name: string,
        data: Array<number>
    }>>([]);
    const [labelsPie, setlabelsPie] = useState<Array<string>>([]);
    const [seriesPie, setSeriesPie] = useState<Array<number>>([]);
    const [openSnack, setOpenSnack] = useState(false);
    const [messageSnack, setMessageSnack] = useState<string>('');

    const wrapper = useRef<JSX.Element>(null);
    const barGraph = useRef<Chart>(null);

    const handleYear = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const target = event.target;
        if (target) {
            const value = target.value ? parseInt(target.value, 10) : undefined;
            setYearInput(value);
        }
    };

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const dataGet = dataStored.some(data => data.year === yearInput);
        if (!dataGet) {
            fetch(
                `http://localhost:8000/random/by-month/${yearInput}`,
                {
                    method: 'GET'
                }
            )
            .then(res => res.json())
            .catch(err => {
                console.log(err);
            })
            .then(response => {
                console.log(response);
                if (response && response.year && response.months) {
                    const data: DataMonths = {
                        year: response.year,
                        months: response.months
                    };
                    console.log('antes del dispatch');
                    dispatch(props.addMonths(data));
                } else {
                    const message = response && response.message ? response.message : 'Error al consultar el año';
                    setMessageSnack(message);
                    setOpenSnack(true);
                }
            });
        } else {
            setMessageSnack('Ya se ha buscado este año');
            setOpenSnack(true);
        }
    };

    const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenSnack(false);
    };

    useEffect(() => {
        if (dataStored && dataStored[0] && dataStored[0].months) {
            const tableDataGet: Array<any> = [];

            dataStored.forEach(data => {
                const newRow = {
                    ...data.months,
                    year: data.year
                }
                tableDataGet.push(newRow);
            });
            setTableData(tableDataGet);
        }
    }, [dataStored]);

    const updateCharts = (event: any, rowData: any) => {
        const dataGet = dataStored.find(data => data.year === rowData.year);
        if (dataGet) {
            const mockOptions = {
                chart: {
                    id: "basic-bar"
                },
                xaxis: {
                    categories: Object.keys(dataGet.months).map(key => getMonthName(key))
                }
            };
            setOptionsBar(mockOptions);
            const mockSeries: Array<{
                name: string,
                data: Array<number>
            }> = [];
            const mockPieLabels: Array<string> = Object.keys(dataGet.months).map(key => getMonthName(key));
            const mockPieSeries: Array<number> = Object.values(dataGet.months);
            mockSeries.push({
                name: `series-${dataGet.year}`,
                data: Object.values(dataGet.months)
            });
    
            setSeriesBar(mockSeries);
            setlabelsPie(mockPieLabels);
            setSeriesPie(mockPieSeries);
        }
    };

    return (
        <>
            <Grid container spacing={3} justify="center">
                <Grid container item xs={12} justify="center" spacing={1} style={
                    {
                        padding: '2em 0'
                    }
                }>
                    <Grid item xs={12} lg={6}>
                        <form noValidate autoComplete="off" onSubmit={handleSubmit}>
                            <TextField
                                id="year"
                                name="year"
                                required
                                label="Ingrese el año a buscar"
                                type="number"
                                onChange={handleYear}
                                style={{
                                    width: '70%',
                                    marginRight: '10%'
                                }}
                            />
                            <Button
                                variant="contained"
                                color="primary"
                                endIcon={<Icon>send</Icon>}
                                type="submit"
                                style={{
                                    width: '20%'
                                }}
                            >
                                Buscar
                            </Button>
                        </form>
                    </Grid>
                </Grid>
                {
                    tableData && tableData[0] ? (
                        <Grid item xs={12}>
                            <div style={{ maxWidth: '100%' }}>
                                <MaterialTable
                                    columns={[
                                        { title: 'Año', field: 'year', type: 'numeric' },
                                        { title: 'Enero', field: 'jan', type: 'numeric' },
                                        { title: 'Febrero', field: 'feb', type: 'numeric' },
                                        { title: 'Marzo', field: 'mar', type: 'numeric' },
                                        { title: 'Abril', field: 'apr', type: 'numeric' },
                                        { title: 'Mayo', field: 'may', type: 'numeric' },
                                        { title: 'Junio', field: 'jun', type: 'numeric' },
                                        { title: 'Julio', field: 'jul', type: 'numeric' },
                                        { title: 'Agosto', field: 'aug', type: 'numeric' },
                                        { title: 'Septiembre', field: 'sep', type: 'numeric' },
                                        { title: 'Octubre', field: 'oct', type: 'numeric' },
                                        { title: 'Noviembre', field: 'nov', type: 'numeric' },
                                        { title: 'Diciembre', field: 'dec', type: 'numeric' },
                                    ]}
                                    data={tableData}
                                    title="Por mes"
                                    actions={[
                                        {
                                          icon: 'visibility',
                                          tooltip: 'Mostrar gráficos',
                                          onClick: updateCharts
                                        },
                                        {
                                          icon: 'delete',
                                          tooltip: 'Borrar fila',
                                          onClick: (event, rowData: any) => dispatch(props.deleteMonths(rowData.year))
                                        }
                                    ]}
                                />
                            </div>
                        </Grid>
                    )
                    :
                    ''
                }
                {
                    optionsBar && seriesBar && seriesBar[0] && labelsPie && seriesPie && seriesPie[0] ? (
                        <>
                            <Grid item xs={6}>
                                <Chart
                                    options={optionsBar}
                                    series={seriesBar}
                                    type="bar"
                                    width="100%"
                                    ref={barGraph}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <Chart
                                    options={{
                                        labels: labelsPie
                                    }}
                                    series={seriesPie}
                                    type="pie"
                                    width="100%"
                                />
                            </Grid>
                        </>
                    ) : ''
                }
            </Grid>
            <Snackbar open={openSnack} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error" ref={wrapper}>
                    {messageSnack}
                </Alert>
            </Snackbar>
        </>
    );
}

export default connector(StatisticsByMonth);
